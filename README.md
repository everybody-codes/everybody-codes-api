# everybody-codes-api

In deze repo combineer ik twee delen van de opdracht: de CLI en de backend.

Normaliter ben ik geen groot fan van JavaScript backends, maar het leek me (gezien de beperkte tijd) efficiënt om helpers van de CLI te kunnen hergebruiken!

### De CLI gebruiken
`node search --name [name]`

### De server starten
`node serve`

De server draait op poort 5000.
