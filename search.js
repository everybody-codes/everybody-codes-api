const fs = require('fs');
const parseCsv = require('./helpers/parseCsv');
const printFilteredResults = require('./helpers/printFilteredResults');

const FILE_PATH = './data/cameras-defb.csv';
const NO_NAME_ERROR = 'Please run me passing a --name arg!';

const nameArgIndex = process.argv.indexOf('--name');
const name = (process.argv[nameArgIndex + 1] || '').toLowerCase();

if (nameArgIndex === -1 || !name) {
  console.log(NO_NAME_ERROR);
  return;
}

const csvFile = fs.readFileSync(FILE_PATH, { encoding: 'utf8' });

printFilteredResults(parseCsv(csvFile), name);
