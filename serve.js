const http = require('http');
const fs = require('fs');
const parseCsv = require('./helpers/parseCsv');

const FILE_PATH = './data/cameras-defb.csv';

const port = 5000;

const requestHandler = (request, response) => {
  response.setHeader('Access-Control-Allow-Origin', '*');
	response.setHeader('Access-Control-Request-Method', '*');
	response.setHeader('Access-Control-Allow-Methods', 'OPTIONS, GET');
	response.setHeader('Access-Control-Allow-Headers', '*');
  
  const csvFile = fs.readFileSync(FILE_PATH, { encoding: 'utf8' });
  response.write(JSON.stringify(parseCsv(csvFile)));
  response.end();
}

const server = http.createServer(requestHandler);

server.listen(port, (err) => {
  if (err) {
    return console.log('something bad happened', err);
  }

  console.log(`API running on http://localhost:${port}`);
})
