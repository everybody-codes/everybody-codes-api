const PREFIX = 'UTR-CM-';
const KEYS = ['name', 'lat', 'lng', 'id'];

const parseCsv = (csv) =>
  csv
  .split('\n')
   // Filter out irrelevant records
  .filter(camera => camera.startsWith(PREFIX))
  // Convert each record to object, for later use
  .map(v => v
    .split(';')
    .reduce((acc, cur, i) => {
      acc[KEYS[i]] = cur;
      return acc;
    }, {})
  )
  // Add an id field in a sloppy way.
  .map(v => ({ id: Number(v.name.replace(PREFIX, '').substr(0,3)), ...v }));

module.exports = parseCsv;
