const objectToString = require('./objectToString');

const NO_RESULTS = 'No results found!';

const printFilteredResults = (results, name) => {
  const filteredResults = results
    .filter(result => result.name.toLowerCase()
    .includes(name));

  if (!filteredResults.length) {
    console.log(NO_RESULTS);
    return;
  }

  filteredResults
    .forEach(result => console.log(objectToString(result)))
};

module.exports = printFilteredResults;
