const objectToString = (obj) =>
  Object.values(obj).join(' | ');

module.exports = objectToString;
